from django.urls import path
from  .views import *

urlpatterns = [
    path('', doctor_home, name='doctor-home'),
    path('patientprofile/<username>/', patientprofile, name='patientprofile'),
    # path('info/', info, name='info'),
    # path('logout/', logout, name='logout'),
]
